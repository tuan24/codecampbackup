const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const mysql = require('mysql2/promise');
const path = require('path');
const render = require('koa-ejs');
const koaBody = require('koa-body');
const bcrypt = require('bcrypt')
const password = 'superman'


//const salesList = require('./saleslist')

const app = new Koa();
const router = new Router();

app.use(koaBody())


app.use(async (ctx, next) => {
    ctx.data = "Hello"
    await next();
 })
 

render(app, {
    root: path.join(process.cwd(), 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
});


router.get('/index',async (ctx,next)=>{
    const data ={title :'index',css : 'css/style.css'}
    await ctx.render('index',data)
    await next;
})

router.post('/completed',async (ctx,next)=>{
    ctx.redirect("/index")
    await next;
})

router.get('/',async (ctx,next)=>{
    const data ={title : 'login',css : 'css/style.css'}
    await ctx.render('login',data)
    await next;
})

router.get('/po_list',async (ctx,next)=>{
    const data ={title : 'PO List',css : 'css/style.css'};
    await ctx.render('po_list',data)
    await next;
})

router.get('/saleslist', async (ctx, next) => {
    const connection = await mysql.createConnection(
        {
            host: 'localhost',
            user: 'root',
            database: 'autopair'
        })

    const [rows, fields] = await connection.query(`SELECT distinct SO, employee, order_date FROM test2 WHERE hub is null`)
    const data ={title : 'Salelist',css : 'css/style2.css', rows};
    await ctx.render('saleslist',data)
    await next
})

router.post('/saleslist', async (ctx, next) => {
    const hub = ctx.request.body.orderHub
    const so = ctx.request.body.orderSo

    const connection = await mysql.createConnection(
        {
            host: 'localhost',
            user: 'root',
            database: 'autopair'
        })

    await connection.query(`UPDATE test2 SET hub=? WHERE SO=?`, [hub, so])
    const [rows, fields] = await connection.query(`SELECT distinct SO, employee, order_date FROM test2 WHERE hub is null`)
    const data ={title : 'Salelist',css : 'css/style2.css', rows};
    await ctx.render('saleslist',data)

    await next
})


router.get('/hub',async (ctx,next)=>{
    const data ={title : 'Hub',css : 'css/hubpage.css'};
    await ctx.render('hub',data)
    console.log(ctx.data)
    await next;
})

router.get('/hublist',async (ctx,next)=>{
    console.log(ctx.data)
    const data ={title : 'Hublist',css : 'css/hublist.css'};
    await ctx.render('hublist',data)
})

router.post('/order',async (ctx,next) => {
    const sale = ctx.request.body.saleID

    const connection = await mysql.createConnection(
        {
            host: 'localhost',
            user: 'root',
            database: 'autopair'
        })

    const [rows, fields] = await connection.query(`SELECT * FROM test2 where SO=?`, sale)
    const [rows2] = await connection.query(`SELECT sum(unit*price) as total_price FROM test2 where SO=?`, sale)
    const data ={title : 'Order',css : 'css/style2.css', rows, rows2}

    await ctx.render('order', data)
})



app.use(serve(path.join(process.cwd(),'public')));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000)